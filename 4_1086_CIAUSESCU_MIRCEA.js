let inputFile = document.getElementById("inputFile");
let butonSalvare = document.getElementById("butonSalvare");
let butonSelectie = document.getElementById("butonSelectie");
let canvas = document.getElementById("canvas");
let context = canvas.getContext("2d");
let butonCrop = document.getElementById("butonCrop");
let butonGrayscale = document.getElementById("butonGrayscale");
let butonStergere = document.getElementById("butonStergere");
let butonRedimensionare = document.getElementById("butonRedimensionare");

let imagine = "";
let canvasW, canvasH;
let initX, initY, finalX, finalY;
let isSelected = false;
let isInSelection = false;


// upload imagine

function seteazaImagine() {
    let maxW = 700, maxH = 700;
    let reader = new FileReader();
    let inputImg = inputFile.files[0];
    if (inputImg) {
        reader.onload = () => {
            imagine = document.createElement("img");
            imagine.onload = () => {

                let ratio = imagine.width / imagine.height;
                // verificare daca imaginea are dimensiunea mai mica decat cea maxima si daca nu micsorarea acesteia prin scalare
                if (imagine.height > maxH) {
                    imagine.height = maxH;
                    imagine.width = maxH * ratio;
                }

                if (imagine.width > maxW) {
                    imagine.width = maxW;
                    imagine.height = maxW / ratio;
                }

                canvas.width = imagine.width;
                canvas.height = imagine.height;
                canvasW = canvas.width;
                canvasH = canvas.height;
                afiseazaImagine();
            };
            imagine.src = reader.result;
        };
        reader.readAsDataURL(inputImg);
    }
}

// afisarea imaginii in canvas
function afiseazaImagine() {
    context.clearRect(0, 0, canvasW, canvasH);
    context.drawImage(imagine, 0, 0, canvasW, canvasH);

    if (isSelected) {
        context.strokeStyle = "rgb(151, 30, 30)";
        context.lineWidth = 1;
        context.strokeRect(initX, initY, finalX - initX, finalY - initY);
    }
}

// salvarea imaginii drept fisier .png
function functieSalvare() {
    let download = document.createElement('a');
    download.href = canvas.toDataURL('image/png');
    download.download = 'edited.png';
    download.click(); // simularea unui click pe link pentru a fi descarcata poza
}

// detectarea daca modul de selectie este activat
function functieSelectare() {
    if (isInSelection === true) {
        isInSelection = false;
        butonSelectie.style.backgroundColor = "rgb(151, 30, 30)";
    } else {
        isInSelection = true;
        initX = initY = finalX = finalY = 0;
        butonSelectie.style.backgroundColor = "rgb(26, 129, 24)";
    }
}

//tratarea evenimentelor de apasare pe canvas
canvas.addEventListener("mousedown", (event) => {
    if (isInSelection) {
        isSelected = true;
        initX = event.clientX - canvas.getBoundingClientRect().left;
        initY = event.clientY - canvas.getBoundingClientRect().top;
    }
});

canvas.addEventListener("mouseup", (event) => {
    if (isInSelection) {
        isSelected = true;
        finalX = event.clientX - canvas.getBoundingClientRect().left;
        finalY = event.clientY - canvas.getBoundingClientRect().top;
        afiseazaImagine(); //afisarea chenarului dupa ce click-ul a fost eliberat
    }
});

// implementarea metodei pentru crop
function functieCrop() {
    if (isSelected) {
        let canvasCrop = document.createElement("canvas"); //crearea unui nou canvas care va contine doar portiunea selectata pentru a o descarca separat
        let contextCrop = canvasCrop.getContext("2d");

        canvasCrop.width = finalX - initX;
        canvasCrop.height = finalY - initY;

        let imagineCrop = context.getImageData(initX, initY, canvasCrop.width, canvasCrop.height);
        contextCrop.putImageData(imagineCrop, 0, 0);

        let download = document.createElement('a');
        download.href = canvasCrop.toDataURL('image/png');
        download.download = 'croppedImage.png';
        download.click();
    }
}

// implementare grayscale
function functieGrayscale() {
    if (isSelected) {
        let initImage = context.getImageData(initX, initY, finalX - initX, finalY - initY);
        let selectie = initImage.data;

        for (let i = 0; i < selectie.length; i += 4) {
            let filtruGray = (selectie[i] + selectie[i + 1] + selectie[i + 2]) / 3;
            selectie[i] = selectie[i + 1] = selectie[i + 2] = filtruGray;
        }
        context.putImageData(initImage, initX, initY);
    }
}

// implementare stergere
function functieStergere() {
    if (isSelected) {
        context.fillStyle = "rgb(255, 255, 255)";
        context.fillRect(initX, initY, finalX - initX, finalY - initY);
    }
}

function functieRedimensionare() {
    let newW = Number(document.getElementById("inputWidth").value);
    let newH = Number(document.getElementById("inputHeight").value); //preluarea lungimii si latimii pozei drept numere pentru a evita probleme cu tipul datelor

    console.log(newW);
    console.log(newH);

    let maxW = 700, maxH = 700;
    if ((newW !== 0 || newH !== 0) && (newW === 0 || newH === 0)) { // verificare daca este doar un camp completat din cele doua (cel gol va avea valoarea 0)

        let ratio = imagine.width / imagine.height;

        // verificare daca imaginea are dimensiunea mai mica decat cea maxima si daca nu micsorarea acesteia prin scalare si setarea campului gol in functie de cel completat
        if (newW !== 0) {
            if (newW > maxW) {
                newW = maxW;
            }
            newH = newW / ratio;
        } else {
            if (newH > maxH) {
                newH = maxH;
            }
            newW = newH * ratio;
        }
        imagine.width = canvas.width = canvasW = newW;
        imagine.height = canvas.height = canvasH = newH;
        afiseazaImagine();
    } else {
        alert("Introduceti doar width sau doar height");
    }
}

inputFile.addEventListener("change", seteazaImagine);
butonSalvare.addEventListener("click", functieSalvare);
butonSelectie.addEventListener("click", functieSelectare);
butonCrop.addEventListener("click", functieCrop);
butonGrayscale.addEventListener("click", functieGrayscale);
butonStergere.addEventListener("click", functieStergere);
butonRedimensionare.addEventListener("click", functieRedimensionare)